//IIFE c передачей глобальных зависимотей (в данном случае библиотека jQuery)
var SelectorRegion = (function ($, regions) {
    'use strict';   //строгий режим проверки синтаксиса

    //параметры модуля
    var options = {
        ui: {
            modalId: '#modal-select-region',
            regionTooltip: '.user-region-tooltip',
            regionSelector: '.user-region-link'
        },
        columns: 2
    };

    //инициализация модуля
    var initialize = function(params) {
        $.extend(options, params);


        if (options.hasOwnProperty('region')) {

            regions.forEach(function (region) {
                //Проверим регион который отдал Яндекс с теми, которые есть у нас в БД
                if (region.attributes.title.indexOf(options.region) >= 0) {
                    $(options.ui.regionSelector)
                        .attr('data-id', region.attributes.id)
                        .text(region.attributes.title);

                    //покажем сообщение что определили регион
                    $(options.ui.regionTooltip).webuiPopover({
						trigger:'manual',
                        title:'Мы угадали Ваш регион?',
                        content: '<button type="submit" class="btn btn-default btn-region-success">Да, спасибо</button> ' +
                                 '<button type="submit" class="btn btn-default btn-region-error">Нет, выбрать другой</button>'
                    });
                    $(options.ui.regionTooltip).webuiPopover('show');
                }
            });
        }

        var columnsOfRegions = _chunkArray(regions, options.columns), html = _renderRegionsColumn(columnsOfRegions);
        $(options.ui.modalId).find('.regions-container').html(html);    //вставим в контейнер

        //если данные были сохранены в localStorage - покажем их
        _getRegionData();

        //слушеам события при нажатии на DOM-элементы
        _setUpListeners();
    };

        /**
         * Навесим события на DOM-элементы
         * @private
         */
    var _setUpListeners = function() {
            //отслеживаем нажатие на регион
            $(document).on('click', '.link-region-item', function (e) {
                //отмена событий по умолчанию при нажатии на ссылку
                e.preventDefault();
                //установим данные в селектор региона и сохраним в локальном хранилище браузера
                _setRegionData({
                    id: $(this).attr('data-id'),
                    title: $(this).text()
                });
                //закроем модальное окно
                $(options.ui.modalId).modal('hide');
            });

            $(document).on('click', '.btn-region-error', function (e) {
                e.preventDefault();
                $(options.ui.regionTooltip).webuiPopover('hide');
                $(options.ui.modalId).modal('show');
            });

            $(document).on('click', '.btn-region-success', function (e) {
                e.preventDefault();

                //TO DO: нужно сохранить данные в локальное хранилище
                $(options.ui.regionTooltip).webuiPopover('hide');
            });

        },

        /**
         * Метод для установки выбранных данных в ссылку-селектор
         * @param options
         */
        _setRegionData = function(data) {
            $(options.ui.regionSelector)
                .attr('data-id', data.id)
                .text(data.title);

            //сохраним регион в localStorage браузера
            localStorage.setItem('regionData', JSON.stringify(data));
        },

        /**
         * Метод для получения данных региона в локальном хранилище
         * что это дает нам?
         * это дает нам сохранение параметров региона даже после обновления страницы!
         * обновите страницу!
         * @private
         */
        _getRegionData = function() {
            var regionDataJSON = localStorage.getItem('regionData');

            if ( !$.isEmptyObject(regionDataJSON) ) {
                var regionData = JSON.parse(regionDataJSON);

                $(options.ui.regionSelector)
                    .attr('data-id', regionData.id)
                    .text(regionData.title);
            }
        },

        /**
         * Метод для разбиения одномерного массива на части
         * @param array
         * @param chunk
         */
        _chunkArray = function(values, chunk) {
            var result = [],
                countRows = Math.ceil(values.length/chunk),
                columnRows = [];

            values.forEach(function(value, index) {
                //нумерация с 0, а нам нужна с 1
                index += 1; var numberOfColumn = Math.ceil((index)/countRows);

                columnRows.push(value);

                //ищем критерий переключения на другой столбец
                if ((index) % (numberOfColumn*countRows) == 0 || index  == values.length) {
                    result.push(columnRows);
                    columnRows = [];
                }
            });

            return result;
        },

        /**
         * Подготовка html-кода списков региона для вставки в DOM-дерево
         * @param columnsOfRegions
         * @returns {string}
         * @private
         */
        _renderRegionsColumn = function(columnsOfRegions) {
            var columns = columnsOfRegions.length,
                innerHTML = '';

            columnsOfRegions.forEach(function(column, index) {
                innerHTML += '<ul class="col-lg-' + Math.ceil(12/columns) + '">';
                column.forEach(function(region) {
                    var id = region.attributes.id,
                        title = region.attributes.title;

                    innerHTML += '<li><a class="link-region-item" href="#" data-id="' + id +  '">' + title + '</li>'
                });
                innerHTML += '</ul>';
            });

            return innerHTML;
        };

    return {
        initialize: initialize,
        chunk: _chunkArray
    }

})(jQuery, ModelRegions);