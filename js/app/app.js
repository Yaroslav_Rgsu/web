(function(SelectorRegion) {
    //ждем когда страница полностью загрузится
    window.onload = function () {
        //если браузер поддерживает HTML5 Geolocation API
        if(navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                var latitude = position.coords.latitude,
                    longitude = position.coords.longitude;

                //проведем обратное геокодирование
                var geocoder = ymaps.geocode([latitude, longitude]);

                //ждем от Яндекса данные, полученные с помощью HTML5 Geolocation API
                geocoder.then(
                    // Геокодер возвращает результаты в виде упорядоченной коллекции GeoObjectArray
                    function (res) {
                        var geoObjects = res.geoObjects,
                            regions = [];

                        //Яндекс может вернуть несколько местоположений, если информация при передаче неточна
                        geoObjects.each(function(object){
                            //по такой длинной цепочке Яндекс хранит наименование региона
                            var metaPropName = 'metaDataProperty.GeocoderMetaData.AddressDetails.Country.AdministrativeArea.AdministrativeAreaName',
                                region = object.properties.get(metaPropName);

                            if (typeof region !== 'undefined') {
                                if (regions.indexOf(region) < 0) {
                                    regions.push(region);
                                }
                            }
                        });

                        var options = {
                            columns: 3
                        };

                        //если хотя бы один регион найден
                        if (regions.length) options.region = regions[0];

                        SelectorRegion.initialize(options);
                    }
                );
            });

        //если браузер не поддерживает HTML5 Geolocation API
        } else {
            //инициализация селектора региона без автоопредления местоположения
            SelectorRegion.initialize({
                columns: 2
            });
        }
    }
})(SelectorRegion);